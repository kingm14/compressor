/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kingm14
 */
public class Writer {
    public void createFile(String write,String file)
    {
        PrintWriter pw=null;
        try {
            pw=new PrintWriter(new FileWriter(file),true);
        } catch (IOException ex) {
            Logger.getLogger(Writer.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("fail");
        }
        pw.print(write);
        pw.close();
    }
}
