/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 

/**
 *
 * @author kingm14
 */
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.*;

public class Reader {

   

    public String readIn(String file) {
        String out = "";
        Scanner s = null;
        try {
            s = new Scanner(new File(file));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("fail");
        }
        while (s.hasNextLine()) {

            String next = s.nextLine();
            String next2="";
            next = next.toLowerCase();
            for (int i = 0; i < next.length(); i++) {
                if(!(next.charAt(i)!=' '&&!Character.isLetter(next.charAt(i))))
                {
                    next2+=""+(char)next.charAt(i);
                }
            }
            
            out += next2;

            if (s.hasNextLine()) {
                out += " ";
            }
        }
        System.out.println(out);
        return out;
    }
    public String readInKeepAll(String file)
    {
        String out = "";
        Scanner s = null;
        try {
            s = new Scanner(new File(file));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("fail");
        }
        while (s.hasNextLine()) {

            String next = s.nextLine();
            
            
            
            out += next;

            
        }
        System.out.println(out);
        return out;
    }
}
