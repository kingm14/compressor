package MyCompressor6;

 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 

/**
 *
 * @author kingm14
 */
import java.util.*;
public class ToBinary {
    
    private int base;
    private int num; 
    private HashMap<String,String> dict;
    private final String bv = "11111111111111111111111111111111";
    
    public ToBinary(int maxInBase)
    {
        base=maxInBase;
        num=26;
        dict=new HashMap<String,String>();
    }
    
    public String getBaseTag()
    {
        return bv.substring(0,base);
    }
    
    public void changeBase(int newBase)
    {
        base=newBase;
    }
    public String convert(String s)
    {
        if(s.length()>1)
        {
             
            //printDict();
            String temp=dict.get(s);
            //System.out.println(temp+":\""+s+"\"");
            for(int i=0; i<base-
            temp.length();)
            {
                temp="0"+temp;
            }
            return temp;
        }
        String ret="";
        
        ret+=convertSingle(s.charAt(0));
        
        return ret;
    }
    public String convertSingle(char c)
    {
        int val=0;
        if(c==' ')
        {
            val=0;
        }
        else
        {
            val=c-'a'+1;
        }
        String s=Integer.toBinaryString(val);
        for(int i=0; i<base-s.length();)
        {
            s="0"+s;
        }
        return s;
    }
    public String convertSingle(int c)
    {
        int val=c;
        
        String s=Integer.toBinaryString(val);
        
        return s;
    }
    public void addToDict(String s)
    {
        if(dict.containsKey(s))
            return;
        num++; 
        dict.put(s, convertSingle(num));
        //System.out.println(s+":"+convertSingle(num)+":"+dict.get(s));
        //String temp=convertSingle(num);
        /*if(temp.indexOf("0")==temp.length()-1)
        {
            base++;
        }*/
    }
    public void printDict()
    {
        Set<String> al= dict.keySet();
        Iterator iter=al.iterator();
        while(iter.hasNext())
        {
            String s=""+iter.next();
            System.out.println(s+":"+dict.get(s));
        }
    }
    public boolean isFull()
    {
        System.out.println(convertSingle(num)+":"+(convertSingle(num).indexOf("0")==convertSingle(num).length()-1));
        return convertSingle(num).indexOf("0")==-1;
    }
    public void incrementBase()
    {
        base++;
    }
}

