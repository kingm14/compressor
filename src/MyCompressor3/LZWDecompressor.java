
/**
 * Write a description of class LZWDecompressor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.*;

public class LZWDecompressor {
    // instance variables - replace the example below with your own

    private String out;
    private FromBinary fb;

    /**
     * Constructor for objects of class LZWDecompressor
     */
    public LZWDecompressor() {
        // initialise instance variables
        fb = new FromBinary();
        /*x = maxNum;
        in=new ArrayList<String>();
        while(input.length()>0)
        {
        in.add(input.substring(0,x));
        input=input.substring(x);
        System.out.println(input);
        }*/
        //System.out.println(decompress());

    }

    public String decompress(String s) {
        out = "";
        int minBits = fb.getMinBits1();
        int count = minBits;
        /*int x = fb.getMinBits1();

        
        String store = "";
        String cur = "";
        String prev = "";
        

        cur = fb.convert(s.substring(0, minBits));
        out += cur;
        prev = store;
        store = cur;
        count += fb.getMinBits1();*/
        String prevCode=s.substring(0, minBits);
        String k=fb.convert(prevCode);
        String k2=fb.convert(prevCode);
        String pattern="";
        
        out+=k;
        while (count < s.length()) {
            
            minBits = fb.getMinBits1();
            
            String currCode= s.substring(count, count + minBits);
            System.out.println(count+":"+currCode);
            if(!fb.contains(currCode))
            {
                pattern=fb.convert(prevCode);
                if(Integer.parseInt(currCode, 2)-fb.getNum()==1)
                {
                    pattern+=fb.convert(k);
                }
                else
                {
                    pattern+=k2;
                
                }
                
            }
            else
            {
                pattern=fb.convert(currCode);
            }
            out+=pattern;
            k=pattern.substring(0,1);
            k2=pattern;
            fb.addToDict(fb.convert(prevCode)+k);
            fb.addToDict(fb.convert(prevCode)+k2);
            prevCode=currCode;
            count+=minBits;
            // printMap();
/*
            minBits = fb.getMinBits1();
            System.out.println("info: " + minBits + ":" + fb.willChangeNext() + ":" + (prev + store) + "\"");
            String toConvert = s.substring(count, count + minBits);
            cur = fb.convert(toConvert);
            int backup=-1;
            if (cur == null) {
                backup=fb.getNum();
                fb.addToDict(null);
                fb.addToDict(prev + store);
                cur = fb.convert(toConvert);
                
            }
            out += cur;
            if(backup!=-1)
                {
                    fb.addToDict(
                        store
                        + cur.substring(0, 1),backup);
                }
            else if (cur.length() > 0)
                {
                    fb.addToDict(
                        store
                        + cur.substring(0, 1));
                    if (prev.length() != 0) 
                        fb.addToDict(prev + store);
                }
            if (cur.length() > 0) {
                prev = store;
                store = cur;
                System.out.println("out:" + out);
            }
            count += minBits;

            //fb.getMinBits();*/
        }
        return out;
    }
        /*while (count < s.length()) {
            // printMap();

            minBits = fb.getMinBits1();
            System.out.println("info: " + minBits + ":" + fb.willChangeNext() + ":" + (prev + store) + "\"");
            String toConvert = s.substring(count, count + minBits);
            cur = fb.convert(toConvert);
            int backup=-1;
            if (cur == null) {
                fb.addToDict(null);
                backup=fb.getNum();
                fb.addToDict(prev + store);
                cur = fb.convert(toConvert);
                
            }

            if (cur.length() > 0) {
                out += cur;
                //System.out.println(fb==null);
                if(backup!=-1)
                {
                    fb.addToDict(
                        store
                        + cur.substring(0, 1),backup);
                }
                else
                {
                    fb.addToDict(
                        store
                        + cur.substring(0, 1));
                    if (prev.length() != 0) {
                    fb.addToDict(prev + store);
                }
                }


                

                prev = store;
                store = cur;
                System.out.println("out:" + out);
            }
            count += minBits;

            //fb.getMinBits();
        }*/

    public void printMap() {
        HashMap map = fb.getMap();
        Set<Integer> set = map.keySet();
        ArrayList<Integer> dict = new ArrayList<Integer>();
        Iterator iter = set.iterator();
        while (iter.hasNext()) {
            dict.add((Integer) iter.next());
        }
        Collections.sort(dict);
        for (int i = 0; i < dict.size(); i++) {
            System.out.println(dict.get(i) + ":" + map.get(dict.get(i)) + "|");
        }
        System.out.println();
    }
}