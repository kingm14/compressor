 
/**
 * Write a description of class FromBinary here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.*;
public class FromBinary
{
    // instance variables - replace the example below with your own
    private int base;
    private HashMap<Integer,String> dict;
    private int num;
    private final String bv = "11111111111111111111111111111111";
    
    public String getBaseTag()
    {
        return bv.substring(0,base);
    }
    /**
     * Constructor for objects of class FromBinary
     */
    public FromBinary()
    {
        num=26;
        base = 5;
        dict=new HashMap<Integer,String>();
    }
    
    public String convert(String s)
    {
        if (s.indexOf("0")==-1)
        {
            base ++;
            return "";
        }
        
        int val=Integer.parseInt(s,2);
        
        if (val==0)
        {
            return " ";
        }
        else if(val<=26)
        {
            return (""+(char)('A'+(val-1)));
        }
        else
        {
            return dict.get(val);
        }
        
    }
    public int getMinBits1()
    {
        //String temp=Integer.toBinaryString(num+1);
        //System.out.println("1minBits: "+temp);
        return base;
        
    }
    public int getMinBits3()
    {
        String temp=Integer.toBinaryString(num+3);
        System.out.println("3minBits: "+temp);
        return temp.length();
        
    }
    public boolean willChangeNext()
    {
        String temp=Integer.toBinaryString(num+1);
        return temp.indexOf("0")==temp.length()-1;
    }
    public boolean willChangeNext2()
    {
        String temp=Integer.toBinaryString(num);
        return temp.indexOf("0")==temp.length()-1;
    }
   
    public void addToDict(String s)
    {
        //System.out.println(dict);
        if(dict.containsValue(s))
        {
            System.out.println("Fail");
            return;
        }
        num++; 
        //System.out.println(num+":\""+s+"\"");
        dict.put(num, s);
        
    }
    public HashMap<Integer,String> getMap()
    {
        return dict;
    }
    public boolean contains(String s)
    {
        //System.out.println(getMap());
        //System.out.println(s+":"+dict.containsValue(s));
        return dict.containsValue(s);
    }
}
