
/**
 * Write a description of class LZWDecompressor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.*;

public class LZWDecompressor {
    // instance variables - replace the example below with your own

    private String out;
    private FromBinary fb;

    /**
     * Constructor for objects of class LZWDecompressor
     */
    public LZWDecompressor() {
        // initialise instance variables
        fb = new FromBinary();
        /*x = maxNum;
        in=new ArrayList<String>();
        while(input.length()>0)
        {
        in.add(input.substring(0,x));
        input=input.substring(x);
        System.out.println(input);
        }*/
        //System.out.println(decompress());

    }

    public String decompress(String s) {
        int x = fb.getMinBits1();

        out = "";
        String store = "";
        String cur = "";
        String prev = "";
        int count = 0;
        int minBits = fb.getMinBits1();

        cur = fb.convert(s.substring(0, minBits));
        out += cur;
        prev = store;
        store = cur;
        count += fb.getMinBits1();

        while (count < s.length()) {
           // printMap();

            minBits = fb.getMinBits1();
            System.out.println("info: " + minBits + ":" + fb.willChangeNext() + ":" + (prev + store) + "\"");
            String toConvert = s.substring(count, count + minBits);
            cur = fb.convert(toConvert);

            if (cur.length() > 0) {
                out += cur;
                //System.out.println(fb==null);

                fb.addToDict(
                        store
                        + cur.substring(0, 1));


                if (prev.length() != 0) {
                    fb.addToDict(prev + store);
                }

                prev = store;
                store = cur;
                System.out.println("out:" + out);
            }
            count += minBits;

            //fb.getMinBits();
        }
        return out;
    }

    public void printMap() {
        HashMap map = fb.getMap();
        Set<Integer> set = map.keySet();
        ArrayList<Integer> dict = new ArrayList<Integer>();
        Iterator iter = set.iterator();
        while (iter.hasNext()) {
            dict.add((Integer) iter.next());
        }
        Collections.sort(dict);
        for (int i = 0; i < dict.size(); i++) {
            System.out.println(dict.get(i) + ":" + map.get(dict.get(i)) + "|");
        }
        System.out.println();
    }
}
