/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package compressor;

/**
 *
 * @author micahking
 */
import java.util.*;
public class ToBinary {

    public String toBinary(String s, int base)
    {
        StringBuilder ret=new StringBuilder();
        char[] cArr=s.toCharArray();
        int[] iArr=new int[cArr.length];
        boolean allReadyB=true;
        for(int i=0; i<iArr.length; i++)
        {
            if((cArr[i]=='0'||cArr[i]=='1'))
            {
                return s;
            }
            if(cArr[i]==' ')
            {
                iArr[i]=0;
            }
            else
                iArr[i]=cArr[i]-'a'+1;
        }
        
        for(int i=0; i<iArr.length; i++)
        {
            String temp=Integer.toBinaryString(iArr[i]);
            for(int j=temp.length(); j<base; j++)
            {
                temp="0"+temp;
            }
            ret.append(temp);
        }
        
        return ret.toString();
                
    }
    public String toBinary(int s, int base)
    {
        String ret="";
        ret=Integer.toBinaryString(s);
        for(int i=ret.length(); i<base; i++)
        {
            ret="0"+ret;
        }
        return ret;
                
    }
    
}
