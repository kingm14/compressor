package MyCompressor5;

 

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 

/**
 *
 * @author kingm14
 */
import java.util.*;
public class LZWCompressor {
    private String in1;
    private ToBinary toB;
    private ArrayList<String> dict;
    public LZWCompressor(String input)
    {
        in1=input;
        toB=new ToBinary(5);
        dict=new ArrayList<String>();
        
    }
    public String compress()
    {
        String in=in1;
        String ret="";
        /*ret+=toB.convert(in.charAt(0)+"");
        toB.addToDict(in.substring(0,2));
        dict.add(in.substring(0,2));
        in=in.substring(1);*/
        String store="";
        String temp="";
        String temp2="";
        while(in.length()>0)
        {
            String largestChunk="";
            largestChunk=in.substring(0,1);
            for(int i=dict.size()-1; i>=0; i--)
            {
                if(in.indexOf(dict.get(i))==0)
                {
                    largestChunk=dict.get(i);
                    i=-1;
                  
                }
            }
            //ret+=toB.convert(largestChunk)+":"+largestChunk+"\n";
            ret+=toB.convert(largestChunk);
            in=in.substring(largestChunk.length());
            if(in.length()!=0)
            {

                temp=store+largestChunk;
                largestChunk+=in.substring(0,1);
                toB.addToDict(largestChunk);
                dict.add(largestChunk);
                 if ((temp.length()>0)&&(dict.size()!=1)&&(!dict.contains(temp)))
                {
                    dict.add(temp);
                    toB.addToDict(temp);
                    System.out.println();
                    System.out.println(temp);
                }              
                if(toB.isFull())
                {
                    ret += toB.getBaseTag();
                    toB.incrementBase();
                }
                store=largestChunk.substring(0,largestChunk.length()-1);
                
            }
            //System.out.println(dict);
        }
        /*String bin=Integer.toBinaryString(dict.size()+26);
        toB=new ToBinary(bin.length());
        for(int i=0; i<bin.length(); i++)
        {
            toB.addToDict(dict.get(i));
        }
        while(in.length()>0)
        {
            String largestChunk="";
            largestChunk=in.substring(0,1);
            for(int i=dict.size()-1; i>=0; i--)
            {
                if(in.indexOf(dict.get(i))==0)
                {
                    largestChunk=dict.get(i);
                    i=-1;
                  
                }
            }
            ret+=toB.convert(largestChunk);
            in=in.substring(largestChunk.length());
            if(in.length()!=0)
            {
                largestChunk+=in.substring(0,1);
                //toB.addToDict(largestChunk);
                dict.add(largestChunk);
            }
            //System.out.println(dict);
        }*/
        
        //toB.printDict();
        System.out.println(ret.length());
        return ret;
    }
    public void printMap()
    {
        for(int i=0; i<dict.size();i++)
        {
            System.out.println(dict.get(i) +":"+toB.convert(dict.get(i)));
        }
    }
}
