/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mycompressor2;

import java.util.*;

/**
 *
 * @author kingm14
 */
public class huffmanEncoder {

    public String compress(String s) {
        String ret = "";
        ArrayList<Chunk> chunks = breakIntoParts(s);
        ArrayList<Chunk> chunksf = new ArrayList<Chunk>();
        ListIterator iter = chunks.listIterator();
        String temp = s;
        ArrayList<TreeNode> fin = new ArrayList<TreeNode>();



        while (!isAllConverted(temp) && iter.hasNext()) {
            Chunk c = (Chunk) iter.next();
            if (temp.indexOf(c.getWord()) != -1) {

                chunksf.add(c);
                fin.add(new TreeNode(c.getWord(), c.getNum()));
                temp = temp.replaceAll(c.getWord(), "ø");

            }
        }

        Collections.sort(fin);
        int numInTree = 0;
        while (fin.size() > 1) {

            /*TreeNode l=fin.get(fin.size()-1);
             TreeNode r=fin.get(fin.size()-2);
             fin.remove(fin.size()-1);*/
            TreeNode temp1 = new TreeNode(fin.remove(fin.size() - 1), fin.remove(fin.size() - 1));
            fin.add(temp1);
            Collections.sort(fin);
        }
        HashMap<String, String> map = fin.get(0).getValues();
        Set<String> sArr = map.keySet();
        Collections.sort(fin, new MyCompare());
        Iterator itr = sArr.iterator();
        ret = s;
        while (itr.hasNext()) {
            String cur = itr.next() + "";
            ret = ret.replaceAll(cur, map.get(cur));

        }
        fin.get(0).printTree();
        return ret;
    }

    public boolean isAllConverted(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != 'ø') {
                return false;
            }
        }
        return true;
    }

    private ArrayList<Chunk> breakIntoParts(String s) {
        ArrayList<Chunk> ret = new ArrayList<Chunk>();
        int window = 1;
        while (window < s.length()) {
            for (int i = 0; i < s.length(); i += 1) {
                if (!(i + window > s.length())) {
                    Chunk c = new Chunk(s.substring(i, i + window));
                    int index = ret.indexOf(c);
                    if (index == -1) {
                        ret.add(c);

                    } else {
                        ret.get(index).increment(1);
                    }
                }

            }
            window++;
        }
        Collections.sort(ret);
        return ret;
    }

    private class Chunk implements Comparable {

        String s;
        int num;

        public Chunk(String word) {
            s = word;
            num = 1;
        }

        public Chunk(String word, int i) {
            s = word;
            num = i;
        }

        public String getWord() {
            return s;
        }

        public int getNum() {
            return num;
        }

        public void increment(int i) {
            num += i;
        }

        @Override
        public int compareTo(Object t) {
            Chunk temp = (Chunk) t;
            if (temp.getNum() == getNum()) {
                return temp.getWord().length() - getWord().length();
            }
            return temp.getNum() - getNum();
        }

        public boolean equals(Object o) {
            Chunk c = (Chunk) o;
            return (c.getWord().equals(getWord()));
        }
    }

    private class MyCompare implements Comparator {

        public int compare(Object t, Object t1) {
            return t.toString().length() - t1.toString().length();
        }
    }

    private class TreeNode implements Comparable {

        private int weight;
        private String s;
        private TreeNode l;
        private TreeNode r;
        private String binary;

        public TreeNode(TreeNode left, TreeNode right) {

            l = left;
            l.addToBinary("0");
            r = right;
            r.addToBinary("1");
            weight = l.getWeight() + r.getWeight();
        }

        public TreeNode(String str, int w) {
            binary = "";
            l = null;
            r = null;
            s = str;
            weight = w;
        }

        public TreeNode() {
            binary = "";
        }

        public void addToBinary(String s) {
            if (l == null && r == null) {
                binary = s+binary;
            } else {
                l.addToBinary(s);
                r.addToBinary(s);
            }

        }

        public int getWeight() {
            return weight;
        }

        public TreeNode getLeft() {
            return l;

        }

        public TreeNode getRight() {
            return r;
        }

        public String getString() {
            return s;
        }

        public HashMap<String, String> getValues() {
            HashMap map = new HashMap<String, String>();
            if (l == null && r == null) {
                map.put(s, binary);
                return map;
            }
            map.putAll(l.getValues());
            map.putAll(r.getValues());
            return map;



        }

        public int compareTo(Object t) {
            TreeNode tn = (TreeNode) (t);
            return tn.getWeight() - getWeight();
        }

        public void printTree() {
            printTree(0);
        }

        public void printTree(int level) {
            if (getLeft() == null && getRight() == null) {
                for (int i = 0; i < level; i++) {
                    System.out.print("     ");
                }
                System.out.println(binary);
            }
            else
            {
                getLeft().printTree(level+1);
                for (int i = 0; i < level; i++) {
                    System.out.print("     ");
                }
                System.out.println(binary);
                getRight().printTree(level+1);
                
            }


        }
    }
    
}
