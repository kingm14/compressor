/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mycompressor2;

/**
 *
 * @author micahking
 */
public class LZ77 {

    private String in;
    private ToBinary toB;
    public LZ77()
    {
       toB = new ToBinary(); 
    }

    public String compress(String input, boolean allReadyBinary) {
        String ret = "";
        
        //in = toB.toBinary(input, 5);
        in=input;
        System.out.println(in);
        ret=compress(allReadyBinary).toString();
        return ret;
    }

    private StringBuilder compress(boolean allReadyBinary) {
        StringBuilder ret = new StringBuilder();
        StringBuilder retFull = new StringBuilder();
        int biggestDist=0;
        int biggestLength=1;
        int iter = 0;
        while (iter < in.length()) {
            System.out.println(retFull);
            int length = 1;
            String cur = in.substring(iter, iter +length);
            String store = in.substring(iter, iter + length);;
            while (retFull.indexOf(cur) != -1&&iter+length<in.length()) {
                store = cur;
                length++;
                cur = in.substring(iter, iter + length);
            }
            System.out.println("\""+store+"\" & \""+cur+"\"");
            int distBack = retFull.length() - retFull.lastIndexOf(store);
            RefPoint brp=new RefPoint(distBack,length-1,store);
            //String comp =  "(" + distBack+ "," + (length-1) +","+store+ ")" ;
            if(distBack>biggestDist)
            {
                biggestDist=distBack;
            }
            if(length-1>biggestLength)
            {
                biggestLength=length-1;
            }
            //String comp = "(" + toB.toBinary(distBack+"", -1) + "," + toB.toBinary(length+"", -1) +","+store+ ")";
            //System.out.println(worthCompressing(store, comp));
            if(length==1)
            {
                ret.append(cur);
                retFull.append(cur);
                iter+=length;
                
            }
            else
            {
                ret.append(new String(":"+brp.toString()+":"));
                retFull.append(store);
                iter+=(length-1);
            }
            /*if (worthCompressing(store, comp)) {
                ret.append(comp);
                retFull.append(store);
                iter+=(length-1);
            }
            else if(length==1)
            {
                ret.append(cur);
                retFull.append(cur);
                iter+=length;
                
            }
            else
            {
                ret.append(store);
                retFull.append(store);
                iter+=(length-1);
            }
            */
            


        }
        
        System.out.println("ret"+ret.toString());
        int b1=Integer.toBinaryString(biggestDist).length();
        
        int b2=Integer.toBinaryString(biggestLength).length();
        System.out.println(b1+","+b2);
        String ret2=ret.toString();
        String[] sArr=ret2.split(":");
        
        StringBuilder ret3=new StringBuilder();
        boolean isFirst=true;
        ret3.append(toB.toBinary(b1,5)+""+toB.toBinary(b2, 5));
        for(int i=0; i<sArr.length; i++)
        {
            
            if(!sArr[i].equals(""))
            {
                if(sArr[i].indexOf("(")!=-1)
                {
                    System.out.println("sArr="+sArr[i]);
                    RefPoint rp=new RefPoint(sArr[i]);
                    if(rp.worthCompressing(b1, b2, false))
                    {
                        
                        
                            ret3.append(rp.toBinary(b1, b2));
                        
                    }
                    else
                    {
                        if(allReadyBinary)
                        {
                            ret3.append(rp.getWord());

                        }
                        else
                        {
                            ret3.append(rp.wordInBinary());
                        }
                    }

                }
                else
                {
                    if(allReadyBinary)
                    {
                        ret3.append(sArr[i]);
                    }
                    else
                    {
                        ret3.append(toB.toBinary(sArr[i], 5));
                    }
                }
                
            }
        }
        return ret3;
    }

    private boolean worthCompressing(String initial, String compressed) {
        System.out.println(initial+":"+compressed);
        return initial.length() > compressed.length();

    }
}
