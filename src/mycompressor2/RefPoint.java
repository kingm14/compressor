/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mycompressor2;
/**
 *
 * @author micahking
 */
public class RefPoint {
    private int dist;
    private int length;
    private String word;
    private ToBinary toB;
    
    public RefPoint(int d, int l, String s)
    {
        dist=d;
        length=l;
        word=s;
        toB=new ToBinary();
    }
    public RefPoint(String s)
    {
        System.out.println(s);
        s=s.substring(1,s.length()-1);
        String[] arr=s.split(",");
        dist=Integer.parseInt(arr[0]);
        length=Integer.parseInt(arr[1]);
        word=(arr[2]);
        toB=new ToBinary();
        
    }
    public String toBinary(int base1, int base2)
    {
        
        String ret="11111"+toB.toBinary(dist, base1)+toB.toBinary(length, base2);
        return ret;
    }
    public String toBinarySepAll(int base1, int base2)
    {
       
        String ret="11111"+toB.toBinary(dist, base1)+"11111"+toB.toBinary(length, base2)+"11111";
        return ret;
    }
    public String toString()
    {
        return "("+dist+","+length+","+word+")";
    }
    public boolean worthCompressing(int b1, int b2, boolean allReadyBinary) {
        String init="";
        String comp="";
        System.out.println(init+":"+comp);
        if(allReadyBinary)
        {
            init+=word;
        }
        else{
            init=wordInBinary();
        }
        comp=toBinary(b1,b2);
        
        return worthCompressing(init,comp);

    }
    private boolean worthCompressing(String initial, String compressed) {
        System.out.println(initial+":"+compressed);
        return initial.length() > compressed.length();

    }
    public String wordInBinary()
    {
        return toB.toBinary(word, 5);
    }
    public String getWord()
    {
        return word;
    }
}
