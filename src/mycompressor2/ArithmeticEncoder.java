/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mycompressor2;

import java.util.*;
import java.math.*;

/**
 *
 * @author micahking
 */
public class ArithmeticEncoder {
    
    private int under_bits;
    
    public ArithmeticEncoder()
    {
        
        under_bits=0;
        
        
    }
    public double compress(String s)
    {
        HashMap<String, Integer> map=getProb(s);
        HashMap<String, Integer> map2=new HashMap<String, Integer>();
        ArrayList<String> set=new ArrayList(map.keySet());
        
        int n=0;
        for(int i=0; i<set.size(); i++)
        {
            //map.put(set.get(i), map.get(set.get(i)));
            int temp=map.get(set.get(i));
            map2.put(set.get(i), n);
            
            n+=temp;
            
        }
        double low=0;
        double high=1;
        for(int i=0; i<s.length(); i++)
        {
            System.out.println(map2.get(s.substring(i, i+1)).doubleValue());
            double intgr=map2.get(s.substring(i, i+1)).doubleValue();
            double l_range=(double) ((intgr+0.0)/(double)s.length());
            System.out.println(map.get(s.substring(i, i+1)));
            double h_range=(l_range+((double)(map.get(s.substring(i, i+1)))/(double)s.length()));
            double range=high-low;
            high=low+range*h_range;
            low=low+range*l_range;
        }
        return (low+high)/2;
        /*BigDecimal l_range;
        BigDecimal h_range;
        BigDecimal number;
        BigDecimal range;
        
        for(int i=0; i<s.length(); i++)
        {
            Integer intgr=map2.get(s.substring(i, i+1));
            l_range=new BigDecimal(intgr/s.length());
            
            h_range=(l_range.add(new BigDecimal(map.get(s.substring(i, i+1)).toString())));
            
            
            
        }*/
        
        
    }
    public HashMap<String,Integer> getProb(String s)
    {
        HashMap<String, Integer> map=new HashMap<String, Integer>();
        for(int i=0; i<s.length();i++)
        {
            if(map.containsKey(s.substring(i,i+1)))
            {
                map.put(s.substring(i,i+1), map.get(s.substring(i,i+1))+1);
            }
            else
            {
                map.put(s.substring(i,i+1), 1);
            }
        }
        return map;
    }
    public 
}
