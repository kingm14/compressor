/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mycompressor2;
/**
 *
 * @author micahking
 */
public class Compressor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        LZ77 lz=new LZ77();
        ToBinary toB=new ToBinary();
        String s=lz.compress("i googled google goggles ",false);
        //String s=lz.compress(toB.toBinary("i googled google goggles ",5),true);
        System.out.println(s);
        
        System.out.println(toB.toBinary("i googled google goggles ", 5));
        
        LZ77Decompressor lzd=new LZ77Decompressor();
        System.out.println("decompressed:"+lzd.decompress(s,false));
        System.out.println("    original:"+("i googled google goggles "));
                
    }
}
