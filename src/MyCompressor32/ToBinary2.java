package MyCompressor32;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 

/**
 *
 * @author kingm14
 */
import java.util.*;
public class ToBinary2 {
    
    private int base;
    private int num; 
    private HashMap<String,String> dict;
    
    public ToBinary2(int maxInBase)
    {
        base=maxInBase;
        num=26;
        dict=new HashMap<String,String>();
    }
    public String convert(String s)
    {
        if(s.length()>1)
        {
             
            return dict.get(s);
        }
        String ret="";
        
        ret+=convertSingle(s.charAt(0));
        
        return ret;
    }
    public String convertSingle(char c)
    {
        int val=0;
        if(c==' ')
        {
            val=0;
        }
        else
        {
            val=c-'a'+1;
        }
        String s=Integer.toBinaryString(val);
        for(int i=0; i<base-s.length();)
        {
            s="0"+s;
        }
        return s;
    }
    public String convertSingle(int c)
    {
        int val=c;
        
        String s=Integer.toBinaryString(val);
        for(int i=0; i<base-s.length();)
        {
            s="0"+s;
        }
        return s;
    }
    public void addToDict(String s)
    {
        num++; 
        dict.put(s, convertSingle(num));
    }
    public void printDict()
    {
        Set<String> al= dict.keySet();
        Iterator iter=al.iterator();
        while(iter.hasNext())
        {
            String s=""+iter.next();
            System.out.println(s+":"+dict.get(s));
        }
    }
}

