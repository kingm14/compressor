package MyCompressor32;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 

/**
 *
 * @author kingm14
 */
public class RLECompressor {
    private String in;
    public RLECompressor(String input)
    {
        in=input;
    }
    public String compress()
    {
        String ret="";
        String prev=in.substring(0, 1);
        int num=1;
        for(int i=1; i<in.length(); i++)
        {
            String cur=in.substring(i,i+1);
            if(cur.equals(prev))
            {
                num++;
            }
            else
            {
                ret+=num;
                num=1;
                prev=cur;
            }
        }  
        ret+=num;
        System.out.println(ret);
        return ret;
    }
}
