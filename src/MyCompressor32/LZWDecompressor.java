package MyCompressor32;

 
/**
 * Write a description of class LZWDecompressor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.*;
public class LZWDecompressor
{
    // instance variables - replace the example below with your own
    
    private String out;
    
    private FromBinary fb;
    /**
     * Constructor for objects of class LZWDecompressor
     */
    public LZWDecompressor()
    {
        // initialise instance variables
        fb=new FromBinary();
        /*x = maxNum;
        in=new ArrayList<String>();
        while(input.length()>0)
        {
            in.add(input.substring(0,x));
            input=input.substring(x);
            System.out.println(input);
        }*/
        //System.out.println(decompress());
        
    }
    public String decompress(String s)
    {
        int x=fb.getMinBits1();
        
        out="";
        String store="";
        String cur="";
        String prev="";
        int count=0;
        
        cur=fb.convert(s.substring(0,fb.getMinBits1()));
        out+=cur;
        prev=store;
        store=cur;
        count+=fb.getMinBits1();
        int minBits=5;
        while(count<s.length())
        {
            printMap();
            
            minBits=fb.getMinBits1();
            System.out.println("info: "+minBits+":"+fb.willChangeNext()+":"+(prev+store)+"\"");
            if(prev.length()!=0&&!fb.contains(prev+store)&&fb.willChangeNext())
            {
                cur=fb.convert(s.substring(count,count+2+minBits));
                System.out.println("sub: "+s.substring(count,count+2+minBits));
                System.out.println("1");
            }
            else
            {
                cur=fb.convert(s.substring(count,count+minBits));
                System.out.println(s.substring(count,count+minBits));
                 System.out.println("2");
            }
            //cur=fb.convert(s.substring(count,count+fb.getMinBits()));
            out+=cur;
            //System.out.println(fb==null);
            
            
            fb.addToDict(
                store+
                cur.substring(0,1));
            
            if(prev.length()!=0)
                fb.addToDict(prev+store);
            
            prev=store;
            store=cur;
            System.out.println("out:"+out);
            minBits=fb.getMinBits1();
            count+=minBits;
            
            //fb.getMinBits();
        }
        return out;
    }
    public void printMap()
    {
        HashMap map=fb.getMap();
       Set<String> set=map.keySet();
       ArrayList<String> dict=new ArrayList<String>();
       Iterator iter=set.iterator();
       while(iter.hasNext())
       {
           dict.add(iter.next().toString());
       }
       Collections.sort(dict);
        for(int i=0; i<dict.size();i++)
        {
            System.out.println(dict.get(i) +":"+map.get(dict.get(i))+"|");
        }
        System.out.println();
    }

    
}
