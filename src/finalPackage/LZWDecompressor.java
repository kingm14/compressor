package finalPackage;

/**
 * Write a description of class LZWDecompressor here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
import java.util.*;

public class LZWDecompressor {
    // instance variables - replace the example below with your own

    private String out;
    private FromBinary fb;

    /**
     * Constructor for objects of class LZWDecompressor
     */
    public LZWDecompressor() {
        // initialise instance variables
        fb = new FromBinary();
        /*x = maxNum;
         in=new ArrayList<String>();
         while(input.length()>0)
         {
         in.add(input.substring(0,x));
         input=input.substring(x);
         System.out.println(input);
         }*/
        //System.out.println(decompress());

    }

    public String decompress(String s) {
        int x = fb.getMinBits1();

        out = "";
        String store = "";
        String cur = "";
        String prev = "";
        int count = 0;
        int minBits = fb.getMinBits1();
        Scanner scanner = new Scanner(System.in);
        cur = fb.convert(s.substring(0, minBits));
        out += cur;
        prev = store;
        store = cur;
        count += fb.getMinBits1();
        int countNum=26;
        while (count < s.length()) {
            //printMap();

            minBits = fb.getMinBits1();
            System.out.println("info: " + minBits + ":" + fb.willChangeNext() + ":" + (prev + store) + "\"");
            String toConvert = s.substring(count, count + minBits);
            System.out.println("cur binary: " + toConvert + ":" + Integer.parseInt(toConvert, 2));
            System.out.println("num=" + fb.getNum());
            int num = fb.getNum();
            boolean erred = false;
            /*if (toConvert.indexOf("0") != -1
                    && Integer.parseInt(toConvert, 2) > num) {
                //System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

                System.out.println("ErroringCase");
             */   printMap();
             /*   String back1 = "";
                String back2 = "";
                back1 = fb.convert(num - 1);
                back2 = fb.convert(num - 2);
                cur = prev + store;
                countNum++;
                fb.addToDict(store+ prev.substring(0, 1),countNum);
                countNum++;
                if (prev.length() != 0) {
                    fb.addToDict(cur,countNum);
                }
                
                //fb.forceAdd(prev+store, Integer.parseInt(toConvert, 2));
                erred = true;//
                System.out.println(cur);
                System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            }
            */
            cur = fb.convert(toConvert);
            
            System.out.println(cur);

            if (cur.length() > 0) {
                out += cur;
                //System.out.println(fb==null);
                //if(!erred){
                countNum++;
                    fb.addToDict(
                            store
                            + cur.substring(0, 1),countNum);

                    countNum++;
                    if (prev.length() != 0) {
                        fb.addToDict(prev + store,countNum);
                    }
                //}

                prev = store;
                store = cur;
                System.out.println("out:" + out);
            }
            count += minBits;
            if (erred) {
                System.out.println(cur);
                //scanner.nextLine();
            }

            //fb.getMinBits();
        }
        printMap();
        return out;
    }

    public void printMap() {
        HashMap map = fb.getMap();
        Set<Integer> set = map.keySet();
        ArrayList<Integer> dict = new ArrayList<Integer>();
        Iterator iter = set.iterator();
        while (iter.hasNext()) {
            dict.add((Integer) iter.next());
        }
        Collections.sort(dict);
        for (int i = 0; i < dict.size(); i++) {
            System.out.println(dict.get(i) + ":" + map.get(dict.get(i)) + "|" + (i % 2));
        }
        System.out.println();
    }
}
